# Bastion OnBoard

![pipeline](https://gitlab.com/riccio8/bastion-onboard/badges/master/pipeline.svg)

[![NuGet](https://img.shields.io/nuget/v/Bastion.OnBoard.Api.svg)](https://www.nuget.org/packages/Bastion.OnBoard.Api/)
[![NuGet](https://img.shields.io/nuget/v/Bastion.OnBoard.EntityFrameworkCore.svg)](https://www.nuget.org/packages/Bastion.OnBoard.EntityFrameworkCore/)
