using Bastion.Authorization;
using Bastion.OnBoard.Abstractions;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.Filters;

using System;
using System.IO;
using System.Linq;

namespace BastionOnBoardSample.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseNpgsql(Configuration.GetConnectionString("pgSqlConnection"),
                b => b.MigrationsAssembly("BastionOnBoardSample.Api")));
            services.AddIdentity<BastionUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddOnBoardService();
            services.AddOnBoardRepository(
                options => options.UseNpgsql(Configuration.GetConnectionString("pgSqlConnection"),
                b => b.MigrationsAssembly("BastionOnBoardSample.Api")));

            services.AddControllers();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Bastion onboard API",
                    Version = "v1",
                    Description = $"Version: <b>{typeof(IOnBoardService).Assembly.GetName().Version.ToString()}</b>",
                });

                Directory.GetFiles(AppContext.BaseDirectory, "*.xml").ToList().ForEach(xmlFilePath => options.IncludeXmlComments(xmlFilePath));

                options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                options.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = JwtBearerDefaults.AuthenticationScheme,
                    Description = "JWT Authorization header using the Bearer scheme.<br/>Example: \"<b>Bearer</b> {token}\""
                });
                options.OperationFilter<SecurityRequirementsOperationFilter>(false, JwtBearerDefaults.AuthenticationScheme);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger().UseSwaggerUI(options =>
            {
                options.RoutePrefix = string.Empty;
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Bastion onboard API V1");
            });
        }
    }
}