﻿using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.EntityFrameworkCore.Contexts;
using Bastion.OnBoard.EntityFrameworkCore.Repositories;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring onBoard repositories.
    /// </summary>
    public static class OnBoardDbContextServiceCollectionExtensions
    {
        /// <summary>
        /// Add onboard repository and dbContext.
        /// </summary>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddOnBoardRepository(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
        {
            return services.AddOnBoardRepository<OnBoardRepository, OnBoardDbContext>(optionsAction);
        }

        /// <summary>
        /// Add custom onboard repository and dbContext.
        /// </summary>
        /// <typeparam name="TOnBoardRepositary">The type of onboard repositary to be registered.</typeparam>
        /// <typeparam name="TOnBoardDbContext">The type of context service to be registered.</typeparam>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddOnBoardRepository<TOnBoardRepositary, TOnBoardDbContext>(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
            where TOnBoardRepositary : OnBoardRepository
            where TOnBoardDbContext : OnBoardDbContext
        {
            services.AddDbContextPool<TOnBoardDbContext>(optionsAction);
            services.TryAddScoped<IOnBoardRepository, TOnBoardRepositary>();

            return services;
        }
    }
}