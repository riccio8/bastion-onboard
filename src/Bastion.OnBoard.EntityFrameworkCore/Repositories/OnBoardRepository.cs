﻿using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.EntityFrameworkCore.Contexts;
using Bastion.OnBoard.Stores.Constants;
using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.OnBoard.EntityFrameworkCore.Repositories
{
    public class OnBoardRepository : IOnBoardRepository
    {
        private readonly OnBoardDbContext dbContext;

        public OnBoardRepository(OnBoardDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<List<DocumentGroup>> GetDocumentGroupsAsync(string clientTypeId)
        {
            return dbContext.DocumentGroup
                .AsNoTracking()
                .Where(e => e.ClientTypeId == clientTypeId)
                .ToListAsync();
        }

        public Task<UserProfileShort> GetProfileAsync(string userId)
        {
            return dbContext.UserProfiles
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.UserId == userId);
        }

        public Task<List<DocumentType>> GetDocumentTypesAsync(string clientType = null)
        {
            return dbContext.DocumentType
                .AsNoTracking()
                .Where(x => clientType == null || x.DocumentInGroup.Any(y => y.DocumentGroup.ClientTypeId == clientType))
                .Include(x => x.DocumentInGroup)
                .ToListAsync();
        }

        public Task<List<DocumentType>> GetAllDocumentTypes()
        {
            return dbContext.DocumentType
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<DocumentType> GetDocumentTypeAsync(string documentTypeId)
        {
            return dbContext.DocumentType
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.DocumentTypeId == documentTypeId);
        }

        public Task<List<Client>> GetClientsAsync()
        {
            return dbContext.Client
                .Include(x => x.User)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<Client> GetClientAsync(string userId)
        {
            return dbContext.Client
                .Include(x => x.User)
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.UserId == userId);
        }

        public async Task<Client> UpdateClientAsync(Client client)
        {
            dbContext.Entry(client).State = EntityState.Modified;
            await dbContext.SaveChangesAsync();

            return client;
        }

        public async Task<Client> AddClientAsync(Client client)
        {
            dbContext.Client.Add(client);
            await dbContext.SaveChangesAsync();

            return client;
        }

        public async Task<Client> DeleteClientAsync(Client client)
        {
            dbContext.Client.Remove(client);
            await dbContext.SaveChangesAsync();

            return client;
        }

        public async Task<Client> ApproveClientAsync(Client client, ClientApproveHistory approveHistory)
        {
            dbContext.ClientApproveHistory.Add(approveHistory);
            dbContext.UserProfiles.Update(client.User);
            dbContext.Client.Remove(client);

            await dbContext.SaveChangesAsync();

            return client;
        }

        public Task<List<ClientDocument>> GetClientDocumentsAsync(string userId)
        {
            return dbContext.ClientDocument
                .AsNoTracking()
                .Where(e => e.UserId == userId)
                .Include(x => x.DocumentType)
                .ThenInclude(x => x.DocumentInGroup)
                .ToListAsync();
        }

        public async Task<ClientDocument> AddClientDocumentAsync(ClientDocument clientDocument)
        {
            dbContext.ClientDocument.Add(clientDocument);
            await dbContext.SaveChangesAsync();

            return clientDocument;
        }

        public async Task<ClientDocument> UpdateClientDocumentAsync(ClientDocument clientDocument, ClientDocumentHistory clientDocumentHistory)
        {
            dbContext.ClientDocument.Update(clientDocument);
            if (clientDocumentHistory != null)
            {
                dbContext.ClientDocumentHistory.Add(clientDocumentHistory);
            }

            await dbContext.SaveChangesAsync();

            return clientDocument;
        }

        public Task<ClientDocument> GetClientDocumentByIdAsync(string userId, string clientDocumentId)
        {
            return dbContext.ClientDocument
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.UserId == userId && e.ClientDocumentId == clientDocumentId);
        }

        public Task<ClientDocument> GetClientDocumentByTypeAsync(string userId, string documentTypeId)
        {
            return dbContext.ClientDocument
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.UserId == userId && e.DocumentTypeId == documentTypeId);
        }

        public Task<int> GetCountClientDocumentsApprovedAsync(string userId)
        {
            return dbContext.ClientDocument
                .AsNoTracking()
                .CountAsync(x => x.UserId == userId && x.DocumentStatusId == DocumentStatusConstants.Approved);
        }

        public Task<List<ClientDocumentHistory>> GetClientDocumentHistoryAsync(string userId, string documentTypeId)
        {
            return dbContext.ClientDocumentHistory
                .AsNoTracking()
                .Where(x => x.UserId == userId && x.DocumentTypeId == documentTypeId)
                .ToListAsync();
        }

        public Task<ClientDocumentHistory> GetClientHistoryDocumentByIdAsync(string userId, string documentTypeId, int historyId)
        {
            return dbContext.ClientDocumentHistory
                .AsNoTracking()
                .Where(x => x.UserId == userId && x.DocumentTypeId == documentTypeId && x.Id == historyId)
                .FirstOrDefaultAsync();
        }
    }
}