﻿using Bastion.OnBoard.EntityFrameworkCore.Configurations;
using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;

namespace Bastion.OnBoard.EntityFrameworkCore.Contexts
{
    public partial class OnBoardDbContext : DbContext
    {
        public OnBoardDbContext(DbContextOptions<OnBoardDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<DocumentGroup> DocumentGroup { get; set; }
        public virtual DbSet<DocumentStatus> DocumentStatus { get; set; }
        public virtual DbSet<DocumentInGroup> DocumentInGroup { get; set; }

        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientDocument> ClientDocument { get; set; }
        public virtual DbSet<UserProfileShort> UserProfiles { get; set; }
        public virtual DbSet<ClientApproveHistory> ClientApproveHistory { get; set; }
        public virtual DbSet<ClientDocumentHistory> ClientDocumentHistory { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DocumentTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentGroupConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentStatusConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentInGroupConfiguration());

            modelBuilder.ApplyConfiguration(new ClientConfiguration());
            modelBuilder.ApplyConfiguration(new ClientDocumentConfiguration());
            modelBuilder.ApplyConfiguration(new UserProfileShortConfiguration());
            modelBuilder.ApplyConfiguration(new ClientApproveHistoryConfiguration());
            modelBuilder.ApplyConfiguration(new ClientDocumentHistoryConfiguration());
        }
    }
}