﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class DocumentGroupConfiguration : IEntityTypeConfiguration<DocumentGroup>
    {
        public void Configure(EntityTypeBuilder<DocumentGroup> builder)
        {
            builder.ToTable("document_group", "onboard");

            builder.Property(e => e.DocumentGroupId)
                .HasColumnName("document_group_id")
                .HasMaxLength(50);

            builder.Property(e => e.ClientTypeId)
                .HasColumnName("client_type_id")
                .HasMaxLength(15)
                .HasDefaultValueSql("'Corporate'::character varying");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

            builder.Property(e => e.SortOrder)
                .HasColumnName("sort_order");
        }
    }
}