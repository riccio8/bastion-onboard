﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class DocumentTypeConfiguration : IEntityTypeConfiguration<DocumentType>
    {
        public void Configure(EntityTypeBuilder<DocumentType> builder)
        {
            builder.ToTable("document", "onboard");

            builder.Property(e => e.DocumentTypeId)
                .HasColumnName("document_id")
                .HasMaxLength(15);

            builder.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);

            builder.Property(e => e.SortOrder)
                .HasColumnName("sort_order");

            builder.Property(e => e.Title)
                .IsRequired()
                .HasColumnName("title")
                .HasMaxLength(255);

            builder.Property(e => e.Url)
                .HasColumnName("url")
                .HasMaxLength(255);
        }
    }
}