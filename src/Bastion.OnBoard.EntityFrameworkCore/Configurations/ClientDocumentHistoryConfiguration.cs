﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class ClientDocumentHistoryConfiguration : IEntityTypeConfiguration<ClientDocumentHistory>
    {
        public void Configure(EntityTypeBuilder<ClientDocumentHistory> builder)
        {
            builder.ToTable("client_document_history", "onboard");

            builder.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValueSql("nextval('onboard.table_name_id_seq'::regclass)");

            builder.Property(e => e.ActionDate)
                .HasColumnName("action_date");

            builder.Property(e => e.ClientDocumentId)
                .IsRequired()
                .HasColumnName("client_document_id")
                .HasMaxLength(30);

            builder.Property(e => e.DocumentTypeId)
                .IsRequired()
                .HasColumnName("document_id")
                .HasMaxLength(15);

            builder.Property(e => e.FileData)
                .IsRequired()
                .HasColumnName("file_data");

            builder.Property(e => e.FileExtension)
                .IsRequired()
                .HasColumnName("file_extension")
                .HasMaxLength(255);

            builder.Property(e => e.FileName)
                .IsRequired()
                .HasColumnName("file_name")
                .HasMaxLength(255);

            builder.Property(e => e.LastComment)
                .HasColumnName("last_comment")
                .HasMaxLength(255);

            builder.Property(e => e.LoadDate)
                .HasColumnName("load_date")
                .HasColumnType("timestamp(6) without time zone")
                .HasDefaultValueSql("now()");

            builder.Property(e => e.OperatorUserId)
                .HasColumnName("operator_user_id")
                .HasMaxLength(36);

            builder.Property(e => e.DocumentStatusId)
                .IsRequired()
                .HasColumnName("status_id")
                .HasMaxLength(15);

            builder.Property(e => e.UserId)
                .IsRequired()
                .HasColumnName("user_id")
                .HasMaxLength(36);

            builder.HasOne(d => d.DocumentType)
                .WithMany(p => p.ClientDocumentHistory)
                .HasForeignKey(d => d.DocumentTypeId)
                .HasConstraintName("client_document_history_document_id_fkey");

            builder.HasOne(d => d.OperatorUser)
                .WithMany(p => p.ClientDocumentHistoryOperatorUser)
                .HasForeignKey(d => d.OperatorUserId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("client_document_history_operator_user_id_fkey");

            builder.HasOne(d => d.DocumentStatus)
                .WithMany(p => p.ClientDocumentHistory)
                .HasForeignKey(d => d.DocumentStatusId)
                .HasConstraintName("client_document_history_status_id_fkey");

            builder.HasOne(d => d.User)
                .WithMany(p => p.ClientDocumentHistoryUser)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("client_document_history_user_id_fkey");
        }
    }
}