﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class ClientConfiguration : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.HasKey(e => e.UserId)
                .HasName("onboarding_client_pkey");

            builder.ToTable("client", "onboard");

            builder.Property(e => e.UserId)
                .HasColumnName("user_id")
                .HasMaxLength(36);

            builder.Property(e => e.BlockedByUserId)
                .HasColumnName("blocked_by_user_id")
                .HasMaxLength(36);

            builder.Property(e => e.ClerkCompleted)
                .HasColumnName("clerk_completed");

            builder.Property(e => e.ControllerComment)
                .HasColumnName("controller_comment")
                .HasMaxLength(1000);

            builder.Property(e => e.ControllerCommentHistory)
                .HasColumnName("controller_comment_history");

            builder.HasOne(d => d.BlockedByUser)
                .WithMany(p => p.ClientBlockedByUser)
                .HasForeignKey(d => d.BlockedByUserId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("client_blocked_by_user_id_fkey");

            builder.HasOne(d => d.User)
                .WithOne(p => p.ClientUser)
                .HasForeignKey<Client>(d => d.UserId)
                .HasConstraintName("onboarding_client_user_id_fkey");
        }
    }
}