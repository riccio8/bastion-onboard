﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class ClientApproveHistoryConfiguration : IEntityTypeConfiguration<ClientApproveHistory>
    {
        public void Configure(EntityTypeBuilder<ClientApproveHistory> builder)
        {
            builder.ToTable("client_approve_history", "onboard");

            builder.Property(e => e.Id)
                .HasColumnName("id")
                .HasDefaultValueSql("nextval('onboard.table_name_id_seq1'::regclass)");

            builder.Property(e => e.ControllerComment)
                .HasColumnName("controller_comment");

            builder.Property(e => e.ControllerCommentHistory)
                .HasColumnName("controller_comment_history");

            builder.Property(e => e.ControllerUserId)
                .IsRequired()
                .HasColumnName("controller_user_id")
                .HasMaxLength(36);

            builder.Property(e => e.Ts)
                .HasColumnName("ts")
                .HasColumnType("timestamp(6) without time zone");

            builder.Property(e => e.UserId)
                .IsRequired()
                .HasColumnName("user_id")
                .HasMaxLength(36);

            builder.HasOne(d => d.ControllerUser)
                .WithMany(p => p.ClientApproveHistoryControllerUser)
                .HasForeignKey(d => d.ControllerUserId)
                .HasConstraintName("client_approve_history_controller_user_id_fkey");

            builder.HasOne(d => d.User)
                .WithMany(p => p.ClientApproveHistoryUser)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("client_approve_history_user_id_fkey");
        }
    }
}