﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class ClientDocumentConfiguration : IEntityTypeConfiguration<ClientDocument>
    {
        public void Configure(EntityTypeBuilder<ClientDocument> builder)
        {
            builder.HasKey(e => new { e.UserId, e.DocumentTypeId })
                .HasName("client_document_pkey");

            builder.ToTable("client_document", "onboard");

            builder.HasIndex(e => new { e.ClientDocumentId, e.UserId })
                .HasName("client_document_client_document_id_idx")
                .IsUnique();

            builder.Property(e => e.UserId)
                .HasColumnName("user_id")
                .HasMaxLength(36);

            builder.Property(e => e.DocumentTypeId)
                .HasColumnName("document_id")
                .HasMaxLength(15);

            builder.Property(e => e.ActionDate)
                .HasColumnName("action_date");

            builder.Property(e => e.ClientDocumentId)
                .IsRequired()
                .HasColumnName("client_document_id")
                .HasMaxLength(30);

            builder.Property(e => e.FileData)
                .IsRequired()
                .HasColumnName("file_data");

            builder.Property(e => e.FileExtension)
                .HasColumnName("file_extension")
                .HasMaxLength(255);

            builder.Property(e => e.FileName)
                .HasColumnName("file_name")
                .HasMaxLength(255);

            builder.Property(e => e.LastComment)
                .HasColumnName("last_comment")
                .HasMaxLength(255);

            builder.Property(e => e.LoadDate)
                .HasColumnName("load_date");

            builder.Property(e => e.DocumentStatusId)
                .IsRequired()
                .HasColumnName("status_id")
                .HasMaxLength(15);

            builder.HasOne(d => d.DocumentType)
                .WithMany(p => p.ClientDocument)
                .HasForeignKey(d => d.DocumentTypeId)
                .HasConstraintName("client_document_form_id_fkey");

            builder.HasOne(d => d.DocumentStatus)
                .WithMany(p => p.ClientDocument)
                .HasForeignKey(d => d.DocumentStatusId)
                .HasConstraintName("client_document_status_id_fkey");

            builder.HasOne(d => d.User)
                .WithMany(p => p.ClientDocument)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("client_document_user_id_fkey");
        }
    }
}