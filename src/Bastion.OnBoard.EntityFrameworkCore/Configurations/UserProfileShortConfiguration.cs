﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class UserProfileShortConfiguration : IEntityTypeConfiguration<UserProfileShort>
    {
        public void Configure(EntityTypeBuilder<UserProfileShort> builder)
        {
            builder.HasKey(e => e.UserId)
                .HasName("user_profile_pkey");

            builder.ToTable("user_profile", "profile");

            builder.Property(e => e.UserId)
                .HasColumnName("user_id")
                .HasMaxLength(36);

            builder.Property(e => e.ClientApproved)
                .HasColumnName("client_approved");

            builder.Property(e => e.ClientTypeId)
                .IsRequired()
                .HasColumnName("client_type_id")
                .HasMaxLength(15)
                .HasDefaultValueSql("'Individual'::character varying")
                .HasComment("Individual, Corporate");

            builder.Property(e => e.Company)
                .HasColumnName("company")
                .HasMaxLength(255);

            builder.Property(e => e.Email)
                .HasColumnName("email")
                .HasMaxLength(255);

            builder.Property(e => e.FirstName)
                .HasColumnName("first_name")
                .HasMaxLength(100);

            builder.Property(e => e.LastName)
                .HasColumnName("last_name")
                .HasMaxLength(100);

            builder.Property(e => e.PhoneNumber)
                .HasColumnName("phone_number")
                .HasMaxLength(255);
        }
    }
}