﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.OnBoard.EntityFrameworkCore.Configurations
{
    internal class DocumentInGroupConfiguration : IEntityTypeConfiguration<DocumentInGroup>
    {
        public void Configure(EntityTypeBuilder<DocumentInGroup> builder)
        {
            builder.HasKey(e => new { e.DocumentTypeId, e.DocumentGroupId })
                .HasName("document_in_group_pkey");

            builder.ToTable("document_in_group", "onboard");

            builder.Property(e => e.DocumentTypeId)
                .HasColumnName("document_id")
                .HasMaxLength(15);

            builder.Property(e => e.DocumentGroupId)
                .HasColumnName("document_group_id")
                .HasMaxLength(50);

            builder.HasOne(d => d.DocumentGroup)
                .WithMany(p => p.DocumentInGroup)
                .HasForeignKey(d => d.DocumentGroupId)
                .HasConstraintName("document_in_group_document_group_id_fkey");

            builder.HasOne(d => d.DocumentType)
                .WithMany(p => p.DocumentInGroup)
                .HasForeignKey(d => d.DocumentTypeId)
                .HasConstraintName("document_in_group_document_id_fkey");
        }
    }
}