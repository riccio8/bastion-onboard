﻿namespace Bastion.OnBoard.Stores.Constants
{
    public static class DocumentStatusConstants
    {
        public const string Review = "REVIEW";
        public const string Approved = "APPROVED";
        public const string Rejected = "REJECTED";
        public const string NotLoaded = "NOT_LOADED";
    }
}