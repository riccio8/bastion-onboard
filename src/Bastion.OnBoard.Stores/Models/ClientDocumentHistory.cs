﻿using Bastion.OnBoard.Stores.Models.Bases;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class ClientDocumentHistory : ClientDocumentBase
    {
        public int Id { get; set; }
        public string OperatorUserId { get; set; }

        public virtual UserProfileShort OperatorUser { get; set; }
        public virtual UserProfileShort User { get; set; }
    }
}