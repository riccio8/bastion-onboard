﻿using System.Collections.Generic;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class Client
    {
        public string UserId { get; set; }
        public bool ClerkCompleted { get; set; }
        public string ControllerComment { get; set; }
        public string BlockedByUserId { get; set; }
        public string ControllerCommentHistory { get; set; }

        public virtual UserProfileShort BlockedByUser { get; set; }
        public virtual UserProfileShort User { get; set; }
        public virtual ICollection<ClientDocument> ClientDocument { get; set; } = new HashSet<ClientDocument>();
    }
}