﻿using System;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class ClientApproveHistory
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string ControllerUserId { get; set; }
        public string ControllerComment { get; set; }
        public DateTime? Ts { get; set; }
        public string ControllerCommentHistory { get; set; }

        public virtual UserProfileShort ControllerUser { get; set; }
        public virtual UserProfileShort User { get; set; }
    }
}