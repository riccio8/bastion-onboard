﻿using System;

namespace Bastion.OnBoard.Stores.Models.Bases
{
    public class ClientDocumentBase
    {
        public string UserId { get; set; }
        public string DocumentTypeId { get; set; }
        public string DocumentStatusId { get; set; }
        public string LastComment { get; set; }
        public string ClientDocumentId { get; set; }
        public DateTime? LoadDate { get; set; }
        public DateTime? ActionDate { get; set; }
#pragma warning disable CA1819 // Properties should not return arrays
        public byte[] FileData { get; set; }
#pragma warning restore CA1819 // Properties should not return arrays
        public string FileName { get; set; }
        public string FileExtension { get; set; }

        public virtual DocumentType DocumentType { get; set; }
        public virtual DocumentStatus DocumentStatus { get; set; }
    }
}