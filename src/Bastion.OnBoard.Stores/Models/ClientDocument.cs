﻿using Bastion.OnBoard.Stores.Models.Bases;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class ClientDocument : ClientDocumentBase
    {
        public virtual Client User { get; set; }
    }
}