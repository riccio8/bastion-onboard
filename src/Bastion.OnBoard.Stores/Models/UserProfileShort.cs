﻿using System.Collections.Generic;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class UserProfileShort
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ClientTypeId { get; set; }
        public bool ClientApproved { get; set; }

        public virtual Client ClientUser { get; set; }
        public virtual ICollection<ClientApproveHistory> ClientApproveHistoryControllerUser { get; set; } = new HashSet<ClientApproveHistory>();
        public virtual ICollection<ClientApproveHistory> ClientApproveHistoryUser { get; set; } = new HashSet<ClientApproveHistory>();
        public virtual ICollection<Client> ClientBlockedByUser { get; set; } = new HashSet<Client>();
        public virtual ICollection<ClientDocumentHistory> ClientDocumentHistoryOperatorUser { get; set; } = new HashSet<ClientDocumentHistory>();
        public virtual ICollection<ClientDocumentHistory> ClientDocumentHistoryUser { get; set; } = new HashSet<ClientDocumentHistory>();
    }
}