﻿using System.Collections.Generic;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class DocumentStatus
    {
        public string DocumentStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientDocument> ClientDocument { get; set; } = new HashSet<ClientDocument>();
        public virtual ICollection<ClientDocumentHistory> ClientDocumentHistory { get; set; } = new HashSet<ClientDocumentHistory>();
    }
}