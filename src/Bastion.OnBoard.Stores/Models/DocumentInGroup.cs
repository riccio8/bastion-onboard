﻿namespace Bastion.OnBoard.Stores.Models
{
    public partial class DocumentInGroup
    {
        public string DocumentTypeId { get; set; }
        public string DocumentGroupId { get; set; }

        public virtual DocumentType DocumentType { get; set; }
        public virtual DocumentGroup DocumentGroup { get; set; }
    }
}