﻿using System.Collections.Generic;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class DocumentGroup
    {
        public string DocumentGroupId { get; set; }
        public string Name { get; set; }
        public short SortOrder { get; set; }
        public string ClientTypeId { get; set; }

        public virtual ICollection<DocumentInGroup> DocumentInGroup { get; set; } = new HashSet<DocumentInGroup>();
    }
}