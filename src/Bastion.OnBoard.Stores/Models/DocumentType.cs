﻿using System.Collections.Generic;

namespace Bastion.OnBoard.Stores.Models
{
    public partial class DocumentType
    {
        public string DocumentTypeId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public short? SortOrder { get; set; }

        public virtual ICollection<ClientDocument> ClientDocument { get; set; } = new HashSet<ClientDocument>();
        public virtual ICollection<ClientDocumentHistory> ClientDocumentHistory { get; set; } = new HashSet<ClientDocumentHistory>();
        public virtual ICollection<DocumentInGroup> DocumentInGroup { get; set; } = new HashSet<DocumentInGroup>();
    }
}