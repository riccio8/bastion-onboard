﻿using Bastion.OnBoard.Stores.Constants;
using Bastion.OnBoard.Stores.Models;

using System;

namespace Bastion.OnBoard.Stores.Extensions
{
    public static class ModelsExtensions
    {
        public static ClientDocument ToClientDocument(
            this DocumentType documentType)
        {
            if (documentType is null)
            {
                throw new ArgumentNullException(nameof(documentType));
            }

            return new ClientDocument
            {
                DocumentTypeId = documentType.DocumentTypeId,
                DocumentStatusId = DocumentStatusConstants.NotLoaded,
            };
        }

        public static ClientDocumentHistory ToClientDocumentHistory(
            this ClientDocument clientDocument,
            string OperatorUserId)
        {
            if (clientDocument is null)
            {
                throw new ArgumentNullException(nameof(clientDocument));
            }

            return new ClientDocumentHistory
            {
                OperatorUserId = OperatorUserId,
                UserId = clientDocument.UserId,
                LoadDate = clientDocument.LoadDate,
                FileData = clientDocument.FileData,
                FileName = clientDocument.FileName,
                ActionDate = clientDocument.ActionDate,
                LastComment = clientDocument.LastComment,
                FileExtension = clientDocument.FileExtension,
                DocumentTypeId = clientDocument.DocumentTypeId,
                ClientDocumentId = clientDocument.ClientDocumentId,
                DocumentStatusId = clientDocument.DocumentStatusId,
            };
        }

        public static ClientDocument UpDateClientDocument(this ClientDocument target, ClientDocument source)
        {
            if (target is null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            target.UserId = source.UserId;
            target.LoadDate = source.LoadDate;
            target.FileData = source.FileData;
            target.FileName = source.FileName;
            target.FileExtension = source.FileExtension;
            target.DocumentTypeId = source.DocumentTypeId;
            target.ClientDocumentId = source.ClientDocumentId;
            target.DocumentStatusId = source.DocumentStatusId;

            return target;
        }
    }
}