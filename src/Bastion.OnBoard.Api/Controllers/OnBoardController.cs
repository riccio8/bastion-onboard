﻿using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.Api.Constants;
using Bastion.OnBoard.Api.Extensions;
using Bastion.OnBoard.Api.Models.Requests;
using Bastion.OnBoard.Api.Models.Responses;
using Bastion.Users.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.OnBoard.Api.Controllers
{
    /// <summary>
    /// User onboard controller.
    /// </summary>
    [Authorize]
    [Route("api/v1/onBoard")]
    [ApiController]
    public class OnBoardController : ControllerBase
    {
        private readonly UsersRepository usersRepository;
        private readonly IOnBoardService onBoardService;
        private readonly SystemAlertService alertService;

        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardController"/>.
        /// </summary>
        public OnBoardController(IOnBoardService onBoardService,
            UsersRepository usersRepository,
            SystemAlertService alertService)
        {
            this.usersRepository = usersRepository;
            this.onBoardService = onBoardService;
            this.alertService = alertService;
        }

        /// <summary>
        /// Get document types.
        /// </summary>
        [HttpGet("documentTypes")]
        public async Task<IEnumerable<OnBoardDocumentTypeResponse>> GetDocumentTypesAsync()
        {
            var UserIdentifier = User.GetIdentifier();
            var userProfile = await usersRepository.GetUserProfile(UserIdentifier);
            var documentTypes = await onBoardService.GetDocumentTypesAsync(userProfile.ClientTypeId);

            return documentTypes.Select(x => x.ToResponse());
        }

        /// <summary>
        /// Get client detail.
        /// </summary>
        [HttpGet("detail")]
        public async Task<OnBoardClientDetailResponse> OperatorGetClientAsync()
        {
            var UserIdentifier = User.GetIdentifier();
            var userProfile = await usersRepository.GetUserProfile(UserIdentifier);
            var client = await onBoardService.GetClientAsync(UserIdentifier, default);
            var groups = await onBoardService.GetDocumentGroupsAsync(userProfile.ClientTypeId);
            var documentTypes = await onBoardService.GetDocumentTypesAsync(userProfile.ClientTypeId);
            var clientDocuments = await onBoardService.Repository.GetClientDocumentsAsync(UserIdentifier);

            return client.ToResponse(UserIdentifier, documentTypes.ToResponse(clientDocuments, groups));
        }

        /// <summary>
        /// Get all client documents.
        /// </summary>
        [HttpGet("documents")]
        public async Task<IEnumerable<OnBoardClientDocumentResponse>> UserGetAllClientDocumentsAsync()
        {
            var userIdentifier = User.GetIdentifier();
            var userProfile = await usersRepository.GetUserProfile(userIdentifier);
            var documentTypes = await onBoardService.GetDocumentTypesAsync(userProfile.ClientTypeId);
            var clientDocuments = await onBoardService.GetClientDocumentsAsync(userIdentifier);

            return documentTypes.ToResponse(clientDocuments);
        }

        /// <summary>
        /// Upload client document.
        /// </summary>
        [Consumes("multipart/form-data")]
        [HttpPost("documents/{documentTypeId}/upload")]
        [RequestSizeLimit(LimitConstants.SizeLimit)]
        [RequestFormLimits(MultipartBodyLengthLimit = LimitConstants.SizeLimit)]
        public async Task<OnBoardClientDocumentResponse> UserUploadClientDocumentAsync(
            string documentTypeId,
            [FromForm] OnBoardUploadDocumentRequest request)
        {
            var userIdentifier = User.GetIdentifier();
            var client = await onBoardService.GetClientAsync(userIdentifier, default);
            var documentType = await onBoardService.GetDocumentTypeAsync(documentTypeId);
            var clientDocument = await onBoardService.AddOrUpdateClientDocumentAsync(userIdentifier, documentType, request.File);
            await alertService.SendDocumentUpdatedAsync(client.User.Email, documentType.Title);
            return documentType.ToResponse(clientDocument);
        }

        /// <summary>
        /// Download client document.
        /// </summary>
        /// <param name="clientDocumentId">Client document id.</param>
        [HttpGet("documents/{clientDocumentId}/download")]
        public async Task<FileStreamResult> UserDownloadClientDocumentAsync(string clientDocumentId)
        {
            var userIdentifier = User.GetIdentifier();

            var clientDocument = await onBoardService.GetClientDocumentByIdAsync(userIdentifier, clientDocumentId);

            return clientDocument.ToFileStream();
        }

        /// <summary>
        /// Get client document history.
        /// </summary>
        /// <param name="documentTypeId">Document type.</param>
        [HttpGet("documents/{documentTypeId}/history")]
        public async Task<IEnumerable<OnBoardClientDocumentHistoryResponse>> UserGetClientDocumentHistoryAsync(string documentTypeId)
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var documentType = await onBoardService.GetDocumentTypeAsync(documentTypeId);
            var documentHistory = await onBoardService.Repository.GetClientDocumentHistoryAsync(operatorUserIdentifier, documentType.DocumentTypeId);

            return documentHistory.Select(x => x.ToResponse());
        }

        /// <summary>
        /// Download client document from history.
        /// </summary>
        /// <param name="historyId">History id.</param>
        /// <param name="documentTypeId">Document type.</param>
        [HttpGet("documents/{documentTypeId}/history/{historyId}/download")]
        public async Task<FileStreamResult> UserDownloadClientDocumentHistoryAsync(
            int historyId,
            string documentTypeId)
        {
            var userIdentifier = User.GetIdentifier();

            var clientDocument = await onBoardService.GetClientHistoryDocumentByIdFromHistoryAsync(userIdentifier, documentTypeId, historyId);

            return clientDocument.ToFileStream();
        }
    }
}