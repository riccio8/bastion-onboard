﻿using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.Api.Constants;
using Bastion.OnBoard.Api.Extensions;
using Bastion.OnBoard.Api.Models.Requests;
using Bastion.OnBoard.Api.Models.Responses;
using Bastion.OnBoard.Stores.Constants;
using Bastion.Users.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace Bastion.OnBoard.Api.Controllers.Clerk
{
    /// <summary>
    /// Clerk onboard controller.
    /// </summary>
    [Route("api/v1/onBoard")]
    [ApiController]
    public class OnBoardController : ControllerBase
    {
        private readonly IOnBoardService onBoardService;
        private readonly SystemAlertService alertService;

        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardController"/>.
        /// </summary>
        public OnBoardController(IOnBoardService onBoardService,
            SystemAlertService alertService)
        {
            this.onBoardService = onBoardService;
            this.alertService = alertService;
        }

        /// <summary>
        /// Block client.
        /// </summary>
        /// <param name="userId">User id.</param>
        [HttpPost("clients/{userId}/block")]
        [Authorize(Roles = RoleConstants.ClerkrOrOperatorOrAdmin)]
        public async Task<OnBoardClientResponse> ClerkBlockClientAsync(string userId)
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var client = await onBoardService.ClientBlockAsync(userId, operatorUserIdentifier);

            return client.ToResponse(operatorUserIdentifier);
        }

        /// <summary>
        /// Unblock client.
        /// </summary>
        /// <param name="userId">User id.</param>
        [HttpPost("clients/{userId}/unblock")]
        [Authorize(Roles = RoleConstants.ClerkrOrOperatorOrAdmin)]
        public async Task<OnBoardClientResponse> ClerkUnblockClientAsync(string userId)
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var client = await onBoardService.ClientUnblockAsync(userId, operatorUserIdentifier);

            return client.ToResponse(operatorUserIdentifier);
        }

        /// <summary>
        /// Approve document.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="documentTypeId">Document type.</param>
        [HttpPost("clients/{userId}/documents/{documentTypeId}/approve")]
        [Authorize(Roles = RoleConstants.ClerkrOrOperatorOrAdmin)]
        public async Task<OnBoardClientDocumentResponse> ClerkApproveDocumentAsync(
            string userId,
            string documentTypeId)
        {
            var operatorUserIdentifier = User.GetIdentifier();
            var client = await onBoardService.GetClientAsync(userId, operatorUserIdentifier);
            var documentType = await onBoardService.GetDocumentTypeAsync(documentTypeId);
            var clientDocument = await onBoardService.SetStatusClientDocumentAsync(
                userId,
                documentType,
                operatorUserIdentifier,
                DocumentStatusConstants.Approved);
            var userName = (client.User.FirstName != null || client.User.LastName != null) ? $"{client.User.FirstName} {client.User.LastName}" : "client";
            await alertService.SendDocumentApprovedAsync(client.User.Email, documentType.Title, userName);
            return documentType.ToResponse(clientDocument);
        }

        /// <summary>
        /// Complete client.
        /// </summary>
        /// <param name="userId">User id.</param>
        [HttpPost("clients/{userId}/complete")]
        [Authorize(Roles = RoleConstants.ClerkrOrOperatorOrAdmin)]
        public async Task<OnBoardClientResponse> ClerkCompleteClientAsync(string userId)
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var client = await onBoardService.ClientCompleteAsync(userId, operatorUserIdentifier);
            await alertService.SendCorpUserNeedReviewAsync(client.User.Email);
            return client.ToResponse(operatorUserIdentifier);
        }

        /// <summary>
        /// Upload client document.
        /// </summary>
        [Consumes("multipart/form-data")]
        [HttpPost("clients/{userId}/documents/{documentTypeId}/upload")]
        [RequestSizeLimit(LimitConstants.SizeLimit)]
        [RequestFormLimits(MultipartBodyLengthLimit = LimitConstants.SizeLimit)]
        [Authorize(Roles = RoleConstants.ClerkrOrOperatorOrAdmin)]
        public async Task<OnBoardClientDocumentResponse> OperatorUploadClientDocumentAsync(
            string userId,
            string documentTypeId,
            [FromForm] OnBoardUploadDocumentRequest request)
        {
            var documentType = await onBoardService.GetDocumentTypeAsync(documentTypeId);
            var clientDocument = await onBoardService.AddOrUpdateClientDocumentAsync(userId, documentType, request.File);

            return documentType.ToResponse(clientDocument);
        }
    }
}