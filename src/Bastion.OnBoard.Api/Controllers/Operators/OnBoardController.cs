﻿using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.Api.Constants;
using Bastion.OnBoard.Api.Extensions;
using Bastion.OnBoard.Api.Models.Requests;
using Bastion.OnBoard.Api.Models.Responses;
using Bastion.OnBoard.Stores.Constants;
using Bastion.Users.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.OnBoard.Api.Controllers.Operators
{
    /// <summary>
    /// Controller onboard controller.
    /// </summary>
    [Route("api/v1/onBoard")]
    [ApiController]
    public class OnBoardController : ControllerBase
    {
        private readonly UsersRepository usersRepository;
        private readonly IOnBoardService onBoardService;
        private readonly SystemAlertService alertService;

        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardController"/>.
        /// </summary>
        public OnBoardController(IOnBoardService onBoardService,
            UsersRepository usersRepository,
            SystemAlertService alertService)
        {
            this.usersRepository = usersRepository;
            this.onBoardService = onBoardService;
            this.alertService = alertService;
        }

        /// <summary>
        /// Reject document.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="documentTypeId">Document type.</param>
        /// <param name="request"></param>
        [HttpPost("clients/{userId}/documents/{documentTypeId}/reject")]
        [Authorize(Roles = RoleConstants.ClerkOrControllerOrOperatorOrAdmin)]
        public async Task<OnBoardClientDocumentResponse> ClerkRejectDocumentAsync(
            string userId,
            string documentTypeId,
            OnBoardRejectRequest request)
        {
            var operatorUserIdentifier = User.GetIdentifier();
            var client = await onBoardService.GetClientAsync(userId, operatorUserIdentifier);
            var documentType = await onBoardService.GetDocumentTypeAsync(documentTypeId);
            var clientDocument = await onBoardService.SetStatusClientDocumentAsync(
                userId,
                documentType,
                operatorUserIdentifier,
                DocumentStatusConstants.Rejected,
                request.Comment);
            var userName = (client.User.FirstName != null || client.User.LastName != null) ? $"{client.User.FirstName} {client.User.LastName}" : "client";
            await alertService.SendDocumentRejectedAsync(client.User.Email, userName, clientDocument.FileName, request.Comment ?? "-");
            return documentType.ToResponse(clientDocument);
        }

        /// <summary>
        /// Get all clients.
        /// </summary>
        [HttpGet("clients")]
        [Authorize(Roles = RoleConstants.ClerkOrControllerOrOperatorOrAdminOrRegulator)]
        public async Task<IEnumerable<OnBoardClientResponse>> OperatorGetClientsAsync()
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var clients = await onBoardService.Repository.GetClientsAsync();

            var isOperator = User.IsInRole(RoleConstants.Admin)
                || User.IsInRole(RoleConstants.Operator)
                || User.IsInRole(RoleConstants.Regulator);
            var isOnlyClerk = !isOperator && User.IsInRole(RoleConstants.Clerk) && !User.IsInRole(RoleConstants.Controller);
            var isOnlyController = !isOperator && User.IsInRole(RoleConstants.Controller) && !User.IsInRole(RoleConstants.Clerk);

            return clients.Where(x => isOperator || (!x.ClerkCompleted && isOnlyClerk) || (x.ClerkCompleted && isOnlyController))
                .OrderBy(x => x.UserId)
                .Select(x => x.ToResponse(operatorUserIdentifier));
        }

        /// <summary>
        /// Get client detail.
        /// </summary>
        /// <param name="userId">User id.</param>
        [HttpGet("clients/{userId}/detail")]
        [Authorize(Roles = RoleConstants.ClerkOrControllerOrOperatorOrAdminOrRegulator)]
        public async Task<OnBoardClientDetailResponse> OperatorGetClientAsync(string userId)
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var client = await onBoardService.GetClientAsync(userId, default);
            var userProfile = await usersRepository.GetUserProfile(userId);
            var groups = await onBoardService.GetDocumentGroupsAsync(userProfile.ClientTypeId);
            var documentTypes = await onBoardService.GetDocumentTypesAsync(userProfile.ClientTypeId);
            var clientDocuments = await onBoardService.Repository.GetClientDocumentsAsync(client.UserId);

            return client.ToResponse(operatorUserIdentifier, documentTypes.ToResponse(clientDocuments, groups));
        }

        /// <summary>
        /// Get all client documents.
        /// </summary>
        /// <param name="userId">User id.</param>
        [HttpGet("clients/{userId}/documents")]
        [Authorize(Roles = RoleConstants.ClerkOrControllerOrOperatorOrAdminOrRegulator)]
        public async Task<IEnumerable<OnBoardClientDocumentResponse>> OperatorGetAllClientDocumentsAsync(string userId)
        {
            var userProfile = await usersRepository.GetUserProfile(userId);
            var documentTypes = await onBoardService.GetDocumentTypesAsync(userProfile.ClientTypeId);
            var clientDocuments = await onBoardService.Repository.GetClientDocumentsAsync(userId);

            return documentTypes.ToResponse(clientDocuments);
        }

        /// <summary>
        /// Get client document history.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="documentTypeId">Document type.</param>
        [HttpGet("clients/{userId}/documents/{documentTypeId}/history")]
        [Authorize(Roles = RoleConstants.ClerkOrControllerOrOperatorOrAdminOrRegulator)]
        public async Task<IEnumerable<OnBoardClientDocumentHistoryResponse>> OperatorGetClientDocumentHistoryAsync(
            string userId,
            string documentTypeId)
        {
            var documentType = await onBoardService.GetDocumentTypeAsync(documentTypeId);
            var documentHistory = await onBoardService.Repository.GetClientDocumentHistoryAsync(userId, documentType.DocumentTypeId);

            return documentHistory.Select(x => x.ToResponse());
        }

        /// <summary>
        /// Download client document from history.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="historyId">History id.</param>
        /// <param name="documentTypeId">Document type.</param>
        [HttpGet("clients/{userId}/documents/{documentTypeId}/history/{historyId}/download")]
        [Authorize(Roles = RoleConstants.ClerkOrControllerOrOperatorOrAdminOrRegulator)]
        public async Task<FileStreamResult> OperatorDownloadClientDocumentHistoryAsync(
            string userId,
            int historyId,
            string documentTypeId)
        {
            var clientDocument = await onBoardService.GetClientHistoryDocumentByIdFromHistoryAsync(userId, documentTypeId, historyId);

            return clientDocument.ToFileStream();
        }

        /// <summary>
        /// Download client document.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="clientDocumentId">Client document id.</param>
        [HttpGet("clients/{userId}/documents/{clientDocumentId}/download")]
        [Authorize(Roles = RoleConstants.ClerkOrControllerOrOperatorOrAdminOrRegulator)]
        public async Task<FileStreamResult> OperatorDownloadClientDocumentAsync(
            string userId,
            string clientDocumentId)
        {
            var clientDocument = await onBoardService.GetClientDocumentByIdAsync(userId, clientDocumentId);

            return clientDocument.ToFileStream();
        }

        /// <summary>
        /// Get all client documents.
        /// </summary>
        [HttpGet("all_bank_documents")]
        public async Task<IEnumerable<OnBoardClientDocumentResponse>> UserGetAllClientDocumentsAsync()
        {
            var documentTypes = await onBoardService.GetAllDocumentTypesAsync();

            // TODO: переделать это все в красивые LINQ
            var lst = new List<OnBoardClientDocumentResponse>();
            foreach (var d in documentTypes)
            {
                if (d.Url != null && !d.Url.ToLower().StartsWith("http"))
                {
                    //yield return new OnBoardClientDocumentResponse(d);
                    lst.Add(new OnBoardClientDocumentResponse(d));
                }
            }

            return lst;
        }
    }
}