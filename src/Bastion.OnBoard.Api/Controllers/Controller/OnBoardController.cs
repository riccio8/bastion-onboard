﻿using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.Api.Constants;
using Bastion.OnBoard.Api.Extensions;
using Bastion.OnBoard.Api.Models.Requests;
using Bastion.OnBoard.Api.Models.Responses;
using Bastion.Users.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace Bastion.OnBoard.Api.Controllers.Controller
{
    /// <summary>
    /// Controller onboard controller.
    /// </summary>
    [Route("api/v1/onBoard")]
    [ApiController]
    public class OnBoardController : ControllerBase
    {
        private readonly IOnBoardService onBoardService;
        private readonly SystemAlertService alertService;

        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardController"/>.
        /// </summary>
        public OnBoardController(IOnBoardService onBoardService,
            SystemAlertService alertService)
        {
            this.onBoardService = onBoardService;
            this.alertService = alertService;
        }

        /// <summary>
        /// Client approve.
        /// </summary>
        /// <param name="userId">User id.</param>
        [HttpPost("clients/{userId}/approve")]
        [Authorize(Roles = RoleConstants.ControllerOrOperatorOrAdmin)]
        public async Task<OnBoardClientResponse> ControllerApproveClientAsync(string userId)
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var client = await onBoardService.ClientApproveAsync(userId, operatorUserIdentifier);
            await alertService.SendCorpUserApprovedAsync(client.User.Email);
            return client.ToResponse(operatorUserIdentifier);
        }

        /// <summary>
        /// Client reject.
        /// </summary>
        [HttpPost("clients/{userId}/reject")]
        [Authorize(Roles = RoleConstants.ControllerOrOperatorOrAdmin)]
        public async Task<OnBoardClientResponse> ControllerRejectClientAsync(
            string userId,
            OnBoardRejectRequest request)
        {
            var operatorUserIdentifier = User.GetIdentifier();

            var client = await onBoardService.ClientRejectAsync(userId, operatorUserIdentifier, request.Comment);
            await alertService.SendCorpUserRejectedAsync(client.User.Email);
            return client.ToResponse(operatorUserIdentifier);
        }
    }
}