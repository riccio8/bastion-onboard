﻿using Bastion.OnBoard.Api.Models.Responses;
using Bastion.OnBoard.Stores.Constants;
using Bastion.OnBoard.Stores.Extensions;
using Bastion.OnBoard.Stores.Models;
using Bastion.OnBoard.Stores.Models.Bases;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bastion.OnBoard.Api.Extensions
{
    public static class ModelsExtensions
    {
        private static readonly FileExtensionContentTypeProvider fileProvider = new FileExtensionContentTypeProvider();

        public static FileStreamResult ToFileStream(
            this ClientDocumentBase clientDocument)
        {
            if (clientDocument is null)
            {
                throw new ArgumentNullException(nameof(clientDocument));
            }

            if (!fileProvider.TryGetContentType(clientDocument.FileExtension, out string contentType))
            {
                contentType = "application/octet-stream";
            }

            return new FileStreamResult(new MemoryStream(clientDocument.FileData), contentType);
        }

        internal static OnBoardClientResponse ToResponse(
            this Client client,
            string operatorUserIdentifier)
        {
            if (client is null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return new OnBoardClientResponse(client, operatorUserIdentifier);
        }

        internal static OnBoardClientDetailResponse ToResponse(
            this Client client,
            string operatorUserIdentifier,
            IDictionary<string, IList<OnBoardClientDocumentResponse>> documents)
        {
            if (client is null)
            {
                throw new ArgumentNullException(nameof(client));
            }

            return new OnBoardClientDetailResponse(client, operatorUserIdentifier, documents);
        }

        internal static OnBoardClientDocumentResponse ToResponse(
            this ClientDocument clientDocument,
            DocumentType documentType)
        {
            if (clientDocument is null)
            {
                throw new ArgumentNullException(nameof(clientDocument));
            }

            if (documentType is null)
            {
                throw new ArgumentNullException(nameof(documentType));
            }

            return new OnBoardClientDocumentResponse(documentType, clientDocument);
        }

        internal static OnBoardClientDocumentResponse ToResponse(
            this DocumentType documentType,
            ClientDocument clientDocument)
        {
            if (documentType is null)
            {
                throw new ArgumentNullException(nameof(documentType));
            }

            return (clientDocument ?? documentType.ToClientDocument()).ToResponse(documentType);
        }

        internal static OnBoardDocumentTypeResponse ToResponse(
            this DocumentType documentType)
        {
            if (documentType is null)
            {
                throw new ArgumentNullException(nameof(documentType));
            }

            return new OnBoardDocumentTypeResponse(documentType);
        }

        public static OnBoardDocumentGroupedTypeResponse ToResponse(
            this DocumentType documentType, string groupName)
        {
            if (documentType is null)
            {
                throw new ArgumentNullException(nameof(documentType));
            }
            if (groupName is null)
            {
                throw new ArgumentNullException(nameof(groupName));
            }

            return new OnBoardDocumentGroupedTypeResponse(documentType, groupName);
        }

        internal static OnBoardClientDocumentHistoryResponse ToResponse(
            this ClientDocumentHistory clientDocumentHistory)
        {
            return new OnBoardClientDocumentHistoryResponse(clientDocumentHistory);
        }

        internal static IEnumerable<OnBoardClientDocumentResponse> ToResponse(
            this IEnumerable<DocumentType> documentTypes,
            IEnumerable<ClientDocument> clientDocuments)
        {
            if (documentTypes is null)
            {
                throw new ArgumentNullException(nameof(documentTypes));
            }

            if (clientDocuments is null)
            {
                throw new ArgumentNullException(nameof(clientDocuments));
            }

            return documentTypes.Select(x => x.ToResponse(clientDocuments.FirstOrDefault(c => c.DocumentTypeId == x.DocumentTypeId)));
        }

        internal static IDictionary<string, IList<OnBoardClientDocumentResponse>> ToResponse(
    this IEnumerable<DocumentType> documentTypes,
    IEnumerable<ClientDocument> clientDocuments,
    IEnumerable<DocumentGroup> groups)
        {
            if (documentTypes is null)
            {
                throw new ArgumentNullException(nameof(documentTypes));
            }

            if (clientDocuments is null)
            {
                throw new ArgumentNullException(nameof(clientDocuments));
            }

            if (groups is null)
            {
                throw new ArgumentNullException(nameof(groups));
            }

            return documentTypes.GroupBy(x => groups.FirstOrDefault(y => x.DocumentInGroup.Any(z => z.DocumentGroupId == y.DocumentGroupId)).Name)
                .ToDictionary(
                    x => x.Key, 
                    x => x.Select(x => x.ToResponse(clientDocuments.FirstOrDefault(c => c.DocumentTypeId == x.DocumentTypeId))).ToList() as IList<OnBoardClientDocumentResponse>);
        }

        internal static async Task<ClientDocument> ToClientDocumentAsync(
            this DocumentType documentType,
            string userIdentifier,
            IFormFile formFile)
        {
            if (documentType is null)
            {
                throw new ArgumentNullException(nameof(documentType));
            }

            if (userIdentifier is null)
            {
                throw new ArgumentNullException(nameof(userIdentifier));
            }

            if (formFile is null)
            {
                throw new ArgumentNullException(nameof(formFile));
            }

            return new ClientDocument
            {
                UserId = userIdentifier,
                LoadDate = DateTime.UtcNow,
                FileName = formFile.FileName,
                DocumentTypeId = documentType.DocumentTypeId,
                FileData = await FormFileToArrayAsync(formFile),
                DocumentStatusId = DocumentStatusConstants.Review,
                ClientDocumentId = "DOC-" + GenerateIdentifier(8),
                FileExtension = Path.GetExtension(formFile.FileName),
            };
        }

        internal static string ToName(this UserProfileShort profile)
        {
            return $"{profile?.FirstName} {profile?.LastName}".Trim();
        }

        internal static string GetIdentifier(this ClaimsPrincipal user)
        {
            var userIdentifier = user?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (userIdentifier is null)
            {
                throw new ArgumentNullException("User indificator is not found.");
            }
            return userIdentifier;
        }

        private static string GenerateIdentifier(int length)
        {
            if (length < 1 || length > 22)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                .Substring(0, length).Replace("/", string.Empty)
                .ToUpperInvariant();
        }

        private static async Task<byte[]> FormFileToArrayAsync(IFormFile formfile)
        {
            if (formfile is null)
            {
                throw new ArgumentNullException(nameof(formfile));
            }

            using (var stream = new MemoryStream())
            {
                await formfile.CopyToAsync(stream);
                return stream.ToArray();
            }
        }
    }
}