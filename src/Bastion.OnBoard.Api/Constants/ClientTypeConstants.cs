﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bastion.OnBoard.Api.Constants
{
    public class ClientTypeConstants
    {
        public const string Corporate = "Corporate";
        public const string Individual = "Individual";
    }
}
