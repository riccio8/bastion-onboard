﻿using Bastion.Authorization;
using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.Api.Constants;
using Bastion.OnBoard.Api.Extensions;
using Bastion.OnBoard.Stores.Constants;
using Bastion.OnBoard.Stores.Extensions;
using Bastion.OnBoard.Stores.Models;
using Bastion.Users.Extensions;
using Bastion.Users.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bastion.OnBoard.Api.Services
{
    /// <summary>
    /// OnBoard service.
    /// </summary>
    public class OnBoardService : IOnBoardService
    {
        private readonly UsersRepository usersRepository;
        private readonly UserManager<BastionUser> userManager;

        /// <summary>
        /// OnBoard repository.
        /// </summary>
        public IOnBoardRepository Repository { get; }

        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardService"/>.
        /// </summary>
        public OnBoardService(
            IOnBoardRepository onBoardRepository,
            UserManager<BastionUser> userManager,
            UsersRepository usersRepository)
        {
            this.usersRepository = usersRepository;
            this.userManager = userManager;
            Repository = onBoardRepository;
        }

        public Task<List<DocumentType>> GetDocumentTypesAsync(string clientType = null)
        {
            return Repository.GetDocumentTypesAsync(clientType);
        }

        public Task<List<DocumentType>> GetAllDocumentTypesAsync()
        {
            return Repository.GetAllDocumentTypes();
        }

        public async Task<DocumentType> GetDocumentTypeAsync(string documentTypeId)
        {
            return await Repository.GetDocumentTypeAsync(documentTypeId)
                ?? throw new Exception($"Unsupported document type '{documentTypeId}'.");
        }

        public async Task<List<DocumentGroup>> GetDocumentGroupsAsync(string clientTypeId)
        {
            return await Repository.GetDocumentGroupsAsync(clientTypeId);
        }

        public Task<List<ClientDocument>> GetClientDocumentsAsync(string userIdentifier)
        {
            return Repository.GetClientDocumentsAsync(userIdentifier);
        }

        public async Task<ClientDocument> GetClientDocumentByIdAsync(string userIdentifier, string clientDocumentId)
        {
            return await Repository.GetClientDocumentByIdAsync(userIdentifier, clientDocumentId)
               ?? throw new Exception($"Document '{clientDocumentId}' is not loaded.");
        }

        public async Task<ClientDocumentHistory> GetClientHistoryDocumentByIdFromHistoryAsync(string userIdentifier, string clientDocumentId, int historyId)
        {
            return await Repository.GetClientHistoryDocumentByIdAsync(userIdentifier, clientDocumentId, historyId)
               ?? throw new Exception($"Document '{clientDocumentId}' is not found in history.");
        }

        public async Task<ClientDocument> AddOrUpdateClientDocumentAsync(string userIdentifier, DocumentType documentType, IFormFile file)
        {
            var client = await Repository.GetClientAsync(userIdentifier)
                ?? throw new Exception("User is not corporate client.");

            var clientDocument = await Repository.GetClientDocumentByTypeAsync(client.UserId, documentType.DocumentTypeId);
            if (clientDocument?.DocumentStatusId == DocumentStatusConstants.Approved)
            {
                throw new Exception($"Document '{documentType.DocumentTypeId}' already approved.");
            }

            var uploadClientDocument = await documentType.ToClientDocumentAsync(client.UserId, file);

            if (clientDocument is null)
            {
                return await Repository.AddClientDocumentAsync(uploadClientDocument);
            }
            else
            {
                return await Repository.UpdateClientDocumentAsync(clientDocument.UpDateClientDocument(uploadClientDocument), default);
            }
        }

        public async Task<ClientDocument> SetStatusClientDocumentAsync(string userIdentifier, DocumentType documentType, string operatorUserIdentifier, string documentStatusId, string comment)
        {
            var client = await GetClientAsync(userIdentifier, operatorUserIdentifier);

            var clientDocument = await Repository.GetClientDocumentByTypeAsync(client.UserId, documentType.DocumentTypeId)
                ?? throw new Exception($"Document '{documentType.DocumentTypeId}' is not loaded.");

            if (clientDocument.DocumentStatusId == documentStatusId)
            {
                throw new Exception("Document already has a status.");
            }

            await UpdateClientDocument(clientDocument, documentStatusId, comment, operatorUserIdentifier);

            return clientDocument;
        }

        private async Task UpdateClientDocument(ClientDocument clientDocument, string documentStatusId, string comment, string operatorUserIdentifier)
        {
            clientDocument.LastComment = comment;
            clientDocument.ActionDate = DateTime.UtcNow;
            clientDocument.DocumentStatusId = documentStatusId;
            await Repository.UpdateClientDocumentAsync(clientDocument, clientDocument.ToClientDocumentHistory(operatorUserIdentifier));
        }

        public async Task ClientCreateAsync(string userIdentifier)
        {
            var user = await userManager.FindByIdAsync(userIdentifier);

            await Repository.AddClientAsync(new Client
            {
                UserId = user.Id,
                ClerkCompleted = false,
            });
            await userManager.AddToRoleAsync(user, RoleConstants.CorporateWaitForApproval);
        }

        public async Task<Client> ClientApproveAsync(string userIdentifier, string operatorUserIdentifier)
        {
            var client = await GetClientAsync(userIdentifier, operatorUserIdentifier);

            client.User.ClientApproved = true;
            await Repository.ApproveClientAsync(client, new ClientApproveHistory
            {
                Ts = DateTime.UtcNow,
                UserId = client.UserId,
                ControllerUserId = operatorUserIdentifier,
                ControllerComment = client.ControllerComment,
                ControllerCommentHistory = client.ControllerCommentHistory,
            });

            var user = await userManager.FindByIdAsync(userIdentifier);
            var userProfile = await usersRepository.GetUserProfile(userIdentifier);
            await userManager.RemoveFromRoleAsync(user, RoleConstants.CorporateWaitForApproval);
            if (userProfile.ClientTypeId == ClientTypeConstants.Corporate)
            {
                await userManager.AddToRolesAsync(user, new string[] { RoleConstants.Corporate, RoleConstants.Client });
            }
            else if (userProfile.ClientTypeId == ClientTypeConstants.Individual)
            {
                await userManager.AddToRoleAsync(user, RoleConstants.Client);
            }
            else
            {
                throw new ArgumentException($"Unknown client type {userProfile.ClientTypeId} (User {userProfile.ToName()}");
            }
            return client;
        }

        public async Task<Client> ClientRejectAsync(string userIdentifier, string operatorUserIdentifier, string comment)
        {
            var client = await GetClientAsync(userIdentifier, operatorUserIdentifier);

            client.ClerkCompleted = false;
            client.User.ClientApproved = false;
            client.ControllerComment = comment;
            client.ControllerCommentHistory = (string.IsNullOrEmpty(client.ControllerCommentHistory)
                ? default : client.ControllerCommentHistory + Environment.NewLine)
                + $"{DateTime.UtcNow:HH:mm yyyy-MM-dd}{Environment.NewLine}{comment}";

            await Repository.UpdateClientAsync(client);

            return client;
        }

        public async Task<Client> ClientCompleteAsync(string userIdentifier, string operatorUserIdentifier)
        {
            var client = await GetClientAsync(userIdentifier, operatorUserIdentifier);

            client.ClerkCompleted = true;
            client.BlockedByUserId = null;
            await Repository.UpdateClientAsync(client);

            return client;
        }

        public async Task<Client> ClientUnblockAsync(string userIdentifier, string operatorUserIdentifier)
        {
            var client = await GetClientAsync(userIdentifier, operatorUserIdentifier);

            client.BlockedByUserId = null;
            await Repository.UpdateClientAsync(client);

            return client;
        }

        public async Task<Client> ClientBlockAsync(string userIdentifier, string operatorUserIdentifier)
        {
            var client = await GetClientAsync(userIdentifier, operatorUserIdentifier);

            client.BlockedByUserId = operatorUserIdentifier;
            await Repository.UpdateClientAsync(client);

            return client;
        }

        public async Task<Client> GetClientAsync(string userIdentifier, string operatorUserIdentifier)
        {
            var client = await Repository.GetClientAsync(userIdentifier)
                ?? throw new Exception("User is not corporate client.");

            if (!string.IsNullOrEmpty(operatorUserIdentifier))
            {
                if (!string.IsNullOrWhiteSpace(client.BlockedByUserId) && client.BlockedByUserId != operatorUserIdentifier)
                {
                    var userProfile = await Repository.GetProfileAsync(client.BlockedByUserId);
                    if (userProfile is null)
                    {
                        throw new Exception("This client blocked by another operator.");
                    }
                    else
                    {
                        throw new Exception($"This client is blocked by operator {userProfile.ToName()}.");
                    }
                }
            }

            return client;
        }
    }
}