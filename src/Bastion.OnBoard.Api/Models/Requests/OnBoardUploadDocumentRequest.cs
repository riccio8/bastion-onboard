﻿using Microsoft.AspNetCore.Http;

using System.ComponentModel.DataAnnotations;

namespace Bastion.OnBoard.Api.Models.Requests
{
    /// <summary>
    /// OnBoard upload document request.
    /// </summary>
    public class OnBoardUploadDocumentRequest
    {
        /// <summary>
        /// File.
        /// </summary>
        [Required]
        public IFormFile File { get; set; }
    }
}