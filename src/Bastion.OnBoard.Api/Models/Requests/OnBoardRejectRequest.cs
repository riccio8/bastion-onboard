﻿namespace Bastion.OnBoard.Api.Models.Requests
{
    /// <summary>
    /// OnBoard reject request.
    /// </summary>
    public class OnBoardRejectRequest
    {
        /// <summary>
        /// Comment.
        /// </summary>
        public string Comment { get; set; }
    }
}