﻿using Bastion.OnBoard.Stores.Models.Bases;

using System;

namespace Bastion.OnBoard.Api.Models.Base
{
    /// <summary>
    /// OnBoard client document base.
    /// </summary>
    public class OnBoardClientDocumentBase
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardClientDocumentBase"/>.
        /// </summary>
        internal OnBoardClientDocumentBase(ClientDocumentBase clientDocumentBase)
        {
            if (clientDocumentBase == null)
                return;
            FileName = clientDocumentBase.FileName;
            LoadDate = clientDocumentBase.LoadDate;
            Comment = clientDocumentBase.LastComment;
            ActionDate = clientDocumentBase.ActionDate;
            Status = clientDocumentBase.DocumentStatusId;
            DocumentType = clientDocumentBase.DocumentTypeId;
        }

        /// <summary>
        /// Status.
        /// </summary>
        public string Status { get; protected set; }

        /// <summary>
        /// Comment.
        /// </summary>
        public string Comment { get; protected set; }

        /// <summary>
        /// FileName.
        /// </summary>
        public string FileName { get; protected set; }

        /// <summary>
        /// LoadDate.
        /// </summary>
        public DateTime? LoadDate { get; protected set; }

        /// <summary>
        /// Document type.
        /// </summary>
        public string DocumentType { get; protected set; }

        /// <summary>
        /// Action date.
        /// </summary>
        public DateTime? ActionDate { get; protected set; }
    }
}