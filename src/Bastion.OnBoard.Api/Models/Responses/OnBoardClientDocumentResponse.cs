﻿using Bastion.OnBoard.Api.Models.Base;
using Bastion.OnBoard.Stores.Models;

namespace Bastion.OnBoard.Api.Models.Responses
{
    /// <summary>
    /// OnBoard client document response.
    /// </summary>
    public class OnBoardClientDocumentResponse : OnBoardClientDocumentBase
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardClientDocumentResponse"/>.
        /// </summary>
        internal OnBoardClientDocumentResponse(
            DocumentType documentType,
            ClientDocument clientDocument) : base(clientDocument)
        {
            Title = documentType.Title;
            FormUrl = documentType.Url;
            Id = clientDocument.ClientDocumentId;
            Description = documentType.Description;
        }
        internal OnBoardClientDocumentResponse(DocumentType documentType) : base(null)
        {
            Title = documentType.Title;
            FormUrl = documentType.Url;
            Description = documentType.Description;
        }

        /// <summary>
        /// Id.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Title.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// From url.
        /// </summary>
        public string FormUrl { get; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; }
    }
}