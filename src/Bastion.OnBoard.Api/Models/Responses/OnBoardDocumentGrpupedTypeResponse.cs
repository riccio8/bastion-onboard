﻿using Bastion.OnBoard.Stores.Models;

namespace Bastion.OnBoard.Api.Models.Responses
{
    /// <summary>
    /// OnBoard document type response.
    /// </summary>
    public class OnBoardDocumentGroupedTypeResponse : OnBoardDocumentTypeResponse
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardDocumentGroupedTypeResponse"/>.
        /// </summary>
        public OnBoardDocumentGroupedTypeResponse(DocumentType documentType, string groupName) : base(documentType)
        {
            this.GroupName = groupName;
        }

        /// <summary>
        /// Group name.
        /// </summary>
        public string GroupName { get; set; }
    }
}