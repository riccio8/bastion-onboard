﻿using Bastion.OnBoard.Api.Extensions;
using Bastion.OnBoard.Stores.Models;

using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Bastion.OnBoard.Api.Models.Responses
{
    /// <summary>
    /// OnBoard client response.
    /// </summary>
    [DataContract]
    public class OnBoardClientResponse
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardClientResponse"/>.
        /// </summary>
        internal OnBoardClientResponse(
            Client client,
            string operatorUserIdentifier)
        {
            Id = client.User.UserId;
            Email = client.User.Email;
            Name = client.User.ToName();
            Company = client.User.Company;
            PhoneNumber = client.User.PhoneNumber;
            ClerkCompleted = client.ClerkCompleted;
            BlockedByUserId = client.BlockedByUserId;
            ControllerComment = client.ControllerComment;
            Blocked = !string.IsNullOrEmpty(client.BlockedByUserId);
            ControllerCommentHistory = client.ControllerCommentHistory;
            Allowed = !Blocked || client.BlockedByUserId == operatorUserIdentifier;
        }

        /// <summary>
        /// Id.
        /// </summary>
        [JsonPropertyName("id")]
        [DataMember(Name = "id")]
        public string Id { get; }

        /// <summary>
        /// Name.
        /// </summary>
        [JsonPropertyName("name")]
        [DataMember(Name = "name")]
        public string Name { get; }

        /// <summary>
        /// Company.
        /// </summary>
        [JsonPropertyName("company")]
        [DataMember(Name = "company")]
        public string Company { get; }

        /// <summary>
        /// Email.
        /// </summary>
        [JsonPropertyName("email")]
        [DataMember(Name = "email")]
        public string Email { get; }

        /// <summary>
        /// Phone number.
        /// </summary>
        [JsonPropertyName("phone")]
        [DataMember(Name = "phone")]
        public string PhoneNumber { get; }

        /// <summary>
        /// Clerk completed.
        /// </summary>
        [JsonPropertyName("clerkCompleted")]
        [DataMember(Name = "clerkCompleted")]
        public bool ClerkCompleted { get; }

        /// <summary>
        /// Blocked.
        /// </summary>
        [JsonPropertyName("blocked")]
        [DataMember(Name = "blocked")]
        public bool Blocked { get; }

        /// <summary>
        /// Allowed.
        /// </summary>
        [JsonPropertyName("allowed")]
        [DataMember(Name = "allowed")]
        public bool Allowed { get; set; }

        /// <summary>
        /// Blocked by user id.
        /// </summary>
        [JsonPropertyName("BlockedByUserId")]
        [DataMember(Name = "BlockedByUserId")]
        public string BlockedByUserId { get; }

        /// <summary>
        /// Controller comment.
        /// </summary>
        [JsonPropertyName("controllerComment")]
        [DataMember(Name = "controllerComment")]
        public string ControllerComment { get; }

        /// <summary>
        /// Controller comment history.
        /// </summary>
        [JsonPropertyName("controllerCommentHistory")]
        [DataMember(Name = "controllerCommentHistory")]
        public string ControllerCommentHistory { get; }
    }
}