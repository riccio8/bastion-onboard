﻿using Bastion.OnBoard.Stores.Models;

namespace Bastion.OnBoard.Api.Models.Responses
{
    /// <summary>
    /// OnBoard document type response.
    /// </summary>
    public class OnBoardDocumentTypeResponse
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardDocumentTypeResponse"/>.
        /// </summary>
        public OnBoardDocumentTypeResponse(DocumentType documentType)
        {
            Url = documentType.Url;
            Title = documentType.Title;
            DocumentId = documentType.DocumentTypeId;
        }

        /// <summary>
        /// Url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Document id.
        /// </summary>
        public string DocumentId { get; set; }
    }
}