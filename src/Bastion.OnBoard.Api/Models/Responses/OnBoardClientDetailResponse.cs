﻿using Bastion.OnBoard.Stores.Models;

using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Bastion.OnBoard.Api.Models.Responses
{
    /// <summary>
    /// OnBoard client group detail response.
    /// </summary>
    [DataContract]
    public class OnBoardClientDetailResponse : OnBoardClientResponse
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardClientDetailResponse"/>.
        /// </summary>
        internal OnBoardClientDetailResponse(
            Client client,
            string operatorIdentifier,
            IDictionary<string, IList<OnBoardClientDocumentResponse>> documents) : base(client, operatorIdentifier)
        {
            Documents = documents;
        }

        /// <summary>
        /// Documents with groups
        /// </summary>
        [JsonPropertyName("documents")]
        [DataMember(Name = "documents")]
        public IDictionary<string, IList<OnBoardClientDocumentResponse>> Documents { get; }
    }
}