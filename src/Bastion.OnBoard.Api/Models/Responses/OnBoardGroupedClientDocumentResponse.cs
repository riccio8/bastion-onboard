﻿using Bastion.OnBoard.Api.Models.Base;
using Bastion.OnBoard.Stores.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Bastion.OnBoard.Api.Models.Responses
{
    /// <summary>
    /// OnBoard grouped client document response.
    /// </summary>
    public class OnBoardGroupedClientDocumentResponse
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardClientDocumentResponse"/>.
        /// </summary>
        internal OnBoardGroupedClientDocumentResponse(
            string group,
            IEnumerable<OnBoardClientDocumentResponse> documents)
        {
            this.Group = group;
            this.Documents = documents;
        }

        /// <summary>
        /// Id.
        /// </summary>
        public string Group { get; }

        /// <summary>
        /// Documents.
        /// </summary>
        [JsonPropertyName("documents")]
        [DataMember(Name = "documents")]
        public IEnumerable<OnBoardClientDocumentResponse> Documents { get; }
    }
}