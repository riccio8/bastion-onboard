﻿using Bastion.OnBoard.Api.Models.Base;
using Bastion.OnBoard.Stores.Models;

namespace Bastion.OnBoard.Api.Models.Responses
{
    /// <summary>
    /// OnBoard client document history response.
    /// </summary>
    public class OnBoardClientDocumentHistoryResponse : OnBoardClientDocumentBase
    {
        /// <summary>
        /// Initializes a new instance of <see cref="OnBoardClientDocumentHistoryResponse"/>.
        /// </summary>
        internal OnBoardClientDocumentHistoryResponse(
            ClientDocumentHistory clientDocumentHistory) : base(clientDocumentHistory)
        {
            Id = clientDocumentHistory.Id;
            LoadDate = clientDocumentHistory.LoadDate;
            Comment = clientDocumentHistory.LastComment;
            ActionDate = clientDocumentHistory.ActionDate;
            Status = clientDocumentHistory.DocumentStatusId;
            DocumentType = clientDocumentHistory.DocumentTypeId;
            OperatorUserId = clientDocumentHistory.OperatorUserId;
        }

        /// <summary>
        /// Id.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Operator user id.
        /// </summary>
        public string OperatorUserId { get; protected set; }
    }
}