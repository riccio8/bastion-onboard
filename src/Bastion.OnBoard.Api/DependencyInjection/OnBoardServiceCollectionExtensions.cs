﻿using Bastion.OnBoard.Abstractions;
using Bastion.OnBoard.Api.Services;

using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring onBoard services.
    /// </summary>
    public static class OnBoardServiceCollectionExtensions
    {
        /// <summary>
        /// Add onBoard service.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        public static IServiceCollection AddOnBoardService(this IServiceCollection services)
        {
            services.AddOnBoardService<OnBoardService>();

            return services;
        }

        /// <summary>
        /// Add onBoard service.
        /// </summary>
        /// <typeparam name="TOnBoardService">The type of onboard service to be registered.</typeparam>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        public static IServiceCollection AddOnBoardService<TOnBoardService>(
            this IServiceCollection services)
            where TOnBoardService : OnBoardService
        {
            services.TryAddScoped<IOnBoardService, TOnBoardService>();

            return services;
        }
    }
}