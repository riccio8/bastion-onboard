﻿using Bastion.OnBoard.Stores.Models;

using Microsoft.AspNetCore.Http;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bastion.OnBoard.Abstractions
{
    /// <summary>
    /// Interface OnBoard service.
    /// </summary>
    public interface IOnBoardService
    {
        IOnBoardRepository Repository { get; }

        Task<List<DocumentGroup>> GetDocumentGroupsAsync(string clientTypeId);

        Task<List<DocumentType>> GetDocumentTypesAsync(string clientType);

        Task<List<DocumentType>> GetAllDocumentTypesAsync();

        Task<DocumentType> GetDocumentTypeAsync(string documentTypeId);

        Task<List<ClientDocument>> GetClientDocumentsAsync(string userIdentifier);

        Task<ClientDocument> GetClientDocumentByIdAsync(string userIdentifier, string clientDocumentId);

        Task<ClientDocument> AddOrUpdateClientDocumentAsync(string userIdentifier, DocumentType documentType, IFormFile file);

        Task<ClientDocumentHistory> GetClientHistoryDocumentByIdFromHistoryAsync(string userIdentifier, string documentTypeId, int historyId);

        Task<ClientDocument> SetStatusClientDocumentAsync(string userIdentifier, DocumentType documentType, string operatorIdentifier, string statusId, string comment = default);

        Task<Client> GetClientAsync(string userIdentifier, string operatorUserIdentifier);

        Task<Client> ClientBlockAsync(string userIdentifier, string operatorIdentifier);

        Task<Client> ClientUnblockAsync(string userIdentifier, string operatorIdentifier);

        Task ClientCreateAsync(string userIdentifier);

        Task<Client> ClientApproveAsync(string userIdentifier, string operatorIdentifier);

        Task<Client> ClientCompleteAsync(string userIdentifier, string operatorIdentifier);

        Task<Client> ClientRejectAsync(string userIdentifier, string operatorIdentifier, string comment);
    }
}