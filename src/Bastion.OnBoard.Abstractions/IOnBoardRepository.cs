﻿using Bastion.OnBoard.Stores.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bastion.OnBoard.Abstractions
{
    /// <summary>
    /// Interface OnBoard repository.
    /// </summary>
    public interface IOnBoardRepository
    {
        Task<List<DocumentGroup>> GetDocumentGroupsAsync(string clientTypeId);

        Task<List<Client>> GetClientsAsync();

        Task<Client> GetClientAsync(string userId);

        Task<Client> AddClientAsync(Client client);

        Task<Client> DeleteClientAsync(Client client);

        Task<Client> UpdateClientAsync(Client client);

        Task<Client> ApproveClientAsync(Client client, ClientApproveHistory approveHistory);

        Task<UserProfileShort> GetProfileAsync(string userId);

        Task<int> GetCountClientDocumentsApprovedAsync(string userId);

        Task<List<ClientDocument>> GetClientDocumentsAsync(string userId);

        Task<ClientDocument> AddClientDocumentAsync(ClientDocument clientDocument);

        Task<ClientDocument> GetClientDocumentByIdAsync(string userId, string clientDocumentId);

        Task<ClientDocument> GetClientDocumentByTypeAsync(string userId, string documentTypeId);

        Task<List<ClientDocumentHistory>> GetClientDocumentHistoryAsync(string userId, string documentTypeId);

        Task<ClientDocumentHistory> GetClientHistoryDocumentByIdAsync(string userId, string documentTypeId, int historyId);

        Task<ClientDocument> UpdateClientDocumentAsync(ClientDocument clientDocument, ClientDocumentHistory clientDocumentHistory);

        Task<List<DocumentType>> GetDocumentTypesAsync(string clientType = null);

        Task<List<DocumentType>> GetAllDocumentTypes();

        Task<DocumentType> GetDocumentTypeAsync(string documentTypeId);
    }
}